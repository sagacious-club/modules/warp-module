package com.gitlab.pk7r.aurora.warp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WarpType {

    PRIVATE("PRIVADA"), PUBLIC("PUBLICA"), TEMPORARY("TEMPORARIA");

    private final String description;

}
