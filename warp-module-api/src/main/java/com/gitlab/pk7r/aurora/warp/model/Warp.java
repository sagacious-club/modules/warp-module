package com.gitlab.pk7r.aurora.warp.model;

import lombok.*;
import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "warp")
public class Warp {

    @Id
    @Column(name = "name", length = 64, nullable = false, updatable = false)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type", nullable = false, columnDefinition = "smallint")
    private WarpType type = WarpType.PRIVATE;

    @Column(name = "location", nullable = false)
    private String location;

}

