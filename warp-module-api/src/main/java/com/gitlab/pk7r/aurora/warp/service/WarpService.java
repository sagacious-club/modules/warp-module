package com.gitlab.pk7r.aurora.warp.service;

import com.gitlab.pk7r.aurora.warp.model.Warp;
import com.gitlab.pk7r.aurora.warp.model.WarpType;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

public interface WarpService {

    List<Warp> getAllWarps();

    String joinWarp(String name, Player player);

    String deleteWarp(String name, Player player);

    Warp createWarp(String name, WarpType warpType, Player player);

    Optional<Warp> getWarp(String name);

}