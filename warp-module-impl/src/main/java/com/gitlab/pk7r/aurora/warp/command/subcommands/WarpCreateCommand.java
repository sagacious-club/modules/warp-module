package com.gitlab.pk7r.aurora.warp.command.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.warp.model.WarpType;
import com.gitlab.pk7r.aurora.warp.service.WarpService;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.val;

public class WarpCreateCommand {

    public static CommandAPICommand warpCreateCommand(Argument warpTypes, WarpService warpService) {
        return new CommandAPICommand("criar")
                .withArguments(new StringArgument("warp"))
                .withArguments(warpTypes)
                .executesPlayer((player, args) -> {
                    val warpName = (String) args[0];
                    val warpType = (String) args[1];
                    switch (warpType) {
                        case "PRIVADA" -> warpService.createWarp(warpName.toLowerCase(), WarpType.PRIVATE, player);
                        case "TEMPORÁRIA" -> warpService.createWarp(warpName.toLowerCase(), WarpType.TEMPORARY, player);
                        default -> warpService.createWarp(warpName.toLowerCase(), WarpType.PUBLIC, player);
                    }
                });
    }

    public static CommandAPICommand warpCreateCommandOneArgument(WarpService warpService) {
        return new CommandAPICommand("criar")
                .withArguments(new StringArgument("warp"))
                .executesPlayer((player, args) -> {
                    val warpName = (String) args[0];
                    warpService.createWarp(warpName.toLowerCase(), WarpType.PUBLIC, player);
                });
    }

    public static CommandAPICommand warpCreateCommandInvalidSyntax() {
        return new CommandAPICommand("criar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.warp.command.criar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());
                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto: /warp criar <nome> <tipo>").formatted());
                });
    }
}