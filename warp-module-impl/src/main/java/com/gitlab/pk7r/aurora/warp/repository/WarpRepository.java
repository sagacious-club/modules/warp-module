package com.gitlab.pk7r.aurora.warp.repository;

import com.gitlab.pk7r.aurora.warp.model.Warp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WarpRepository extends JpaRepository<Warp, String> {

}
