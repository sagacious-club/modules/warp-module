package com.gitlab.pk7r.aurora.warp.command;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.warp.command.subcommands.WarpCreateCommand;
import com.gitlab.pk7r.aurora.warp.command.subcommands.WarpDeleteCommand;
import com.gitlab.pk7r.aurora.warp.model.Warp;
import com.gitlab.pk7r.aurora.warp.model.WarpType;
import com.gitlab.pk7r.aurora.warp.service.WarpService;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Command
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class WarpCommand {

    WarpService warpService;

    @PostConstruct
    public void warpCommand() {
        val warps = new StringArgument("warp").includeSuggestions(ArgumentSuggestions.strings(info -> warpService.getAllWarps()
                .stream()
                .filter(warp -> !warp.getType().equals(WarpType.PRIVATE) ||
                        info.sender().hasPermission("aurora.warp.join." + warp.getName()))
                .map(Warp::getName)
                .toArray(String[]::new)));
        val warpTypes = new GreedyStringArgument("tipo").replaceSuggestions(ArgumentSuggestions.strings("PRIVADA", "TEMPORÁRIA", "PÚBLICA"));
        new CommandAPICommand("warp")
                .executesPlayer((player, args) -> {
                    player.spigot().sendMessage(new Message("&cUso correto: /warp <warp>").formatted());
                }).register();
        new CommandAPICommand("warp")
                .withArguments(warps)
                .executesPlayer((player, args) -> {
                    val warpName = (String)  args[0];
                    player.spigot().sendMessage(new Message(warpService.joinWarp(warpName, player)).formatted());
                })
                .withSubcommand(WarpCreateCommand.warpCreateCommand(warpTypes, warpService))
                .withSubcommand(WarpCreateCommand.warpCreateCommandOneArgument(warpService))
                .withSubcommand(WarpCreateCommand.warpCreateCommandInvalidSyntax())
                .withSubcommand(WarpDeleteCommand.warpDeleteCommand(warpService, warps))
                .withSubcommand(WarpDeleteCommand.warpDeleteCommandInvalidSyntax())
                .register();
    }
}