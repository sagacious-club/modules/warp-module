package com.gitlab.pk7r.aurora.warp.command.subcommands;

import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.warp.service.WarpService;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import lombok.val;

public class WarpDeleteCommand {

    public static CommandAPICommand warpDeleteCommand(WarpService warpService, Argument warps) {
        return new CommandAPICommand("deletar")
                .withArguments(warps)
                .executesPlayer((player, args) -> {
                    val warpName = (String) args[0];
                    player.spigot().sendMessage(new Message(warpService.deleteWarp(warpName, player)).formatted());
                });
    }

    public static CommandAPICommand warpDeleteCommandInvalidSyntax() {
        return new CommandAPICommand("deletar")
                .executesPlayer((player, args) -> {
                    if (!player.hasPermission("aurora.warp.command.deletar")) {
                        player.spigot().sendMessage(new Message("&cSem permissão.").formatted());

                        return;
                    }
                    player.spigot().sendMessage(new Message("&cUso correto: /warp deletar <nome>").formatted());
                });
    }
}
