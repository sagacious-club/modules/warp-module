package com.gitlab.pk7r.aurora.warp.service;

import com.gitlab.pk7r.aurora.exception.ExecutionException;
import com.gitlab.pk7r.aurora.execution.Execution;
import com.gitlab.pk7r.aurora.service.LocationSerializationService;
import com.gitlab.pk7r.aurora.service.SchedulerService;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.warp.model.Warp;
import com.gitlab.pk7r.aurora.warp.model.WarpType;
import com.gitlab.pk7r.aurora.warp.repository.WarpRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.entity.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class WarpServiceImpl implements WarpService {

    Execution execution;

    SchedulerService schedulerService;

    WarpRepository warpRepository;

    LocationSerializationService locationSerializationService;

    @PostConstruct
    public void onInit() {
        schedulerService.scheduleSyncDelayedTask(() ->
                getAllWarps().stream().filter(warp -> warp.getType() == WarpType.TEMPORARY).forEach(warp -> warpRepository.delete(warp)), 60L);
    }

    @Override
    public List<Warp> getAllWarps() {
        return warpRepository.findAll();
    }

    @Override
    public String joinWarp(String name, Player player) {
        val warp = getWarp(name);
        if (warp.isEmpty()) {
            return "&cEssa warp não existe.";
        }
        if (!player.hasPermission("aurora.warp.join." + warp.get().getName()) && warp.get().getType().equals(WarpType.PRIVATE)) return "&cSem permissão.";
        val location = locationSerializationService.deserializeLocation(warp.get().getLocation());
        player.teleport(location);
        player.getLocation().setPitch(location.getPitch());
        player.getLocation().setYaw(location.getYaw());
        return String.format("&aTeleportado para a warp &e%s&a.", warp.get().getName());
    }

    @Override
    public String deleteWarp(String name, Player player) {
        return execution.run(() -> {
            if (!player.hasPermission("aurora.warp.command.deletar")) throw new ExecutionException(player, "Sem permissão.");
            val warp = getWarp(name).orElseThrow(() -> new ExecutionException(player, "Essa warp não existe."));
            warpRepository.delete(warp);
            return String.format("&aWarp &e%s &adeletada com sucesso.", warp.getName());
        });
    }

    @Override
    public Warp createWarp(String name, WarpType warpType, Player player) {
        return execution.run(() -> {
            if (!player.hasPermission("aurora.warp.command.criar")) throw new ExecutionException(player, "Sem permissão.");
            val warp = getWarp(name).orElse(new Warp());
            warp.setName(name.toLowerCase());
            warp.setType(warpType);
            warp.setLocation(locationSerializationService.serializeLocation(player.getLocation()));
            warpRepository.save(warp);
            player.spigot().sendMessage(new Message(String.format("&aWarp &e%s &acriada com sucesso.", warp.getName())).formatted());
            return warp;
        });
    }

    @Override
    public Optional<Warp> getWarp(String name) {
        return warpRepository.findById(name);
    }
}