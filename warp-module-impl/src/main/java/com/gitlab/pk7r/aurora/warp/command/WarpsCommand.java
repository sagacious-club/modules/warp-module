package com.gitlab.pk7r.aurora.warp.command;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.util.Message;
import com.gitlab.pk7r.aurora.warp.model.WarpType;
import com.gitlab.pk7r.aurora.warp.service.WarpService;
import dev.jorel.commandapi.CommandAPICommand;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;

@Command
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class WarpsCommand {

    WarpService warpService;

    @PostConstruct
    public void warpsCommand() {
        new CommandAPICommand("warps")
                .executesPlayer((player, args) -> {
                    val message = warpService.getAllWarps().stream()
                            .filter(warp -> !warp.getType().equals(WarpType.PRIVATE) ||
                                    player.hasPermission("aurora.warp.join." + warp.getName()))
                            .map(warp -> String.format("&e[%s](/warp %s hover=&7Visibilidade: &6%s)"
                                    , warp.getName(), warp.getName(), warp.getType().getDescription()))
                            .collect(Collectors.collectingAndThen(Collectors.joining("&6, &e"), result ->
                                    result.isEmpty() ? "&cNenhuma warp disponível." : "&aLista de Warps:\n" + result));
                    player.spigot().sendMessage(new Message(message).formatted());
                }).register();
    }
}