CREATE TABLE  if not exists warp
(
    name     VARCHAR(64)  NOT NULL,
    type     SMALLINT     NOT NULL,
    location VARCHAR(255) NOT NULL,
    CONSTRAINT pk_warp PRIMARY KEY (name)
);